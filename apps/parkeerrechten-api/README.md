# Lokale benodigheden
- minikube
- [Install CloudNativePG in Minikube](https://cloudnative-pg.io/documentation/1.22/installation_upgrade/)
    ```
    kubectl apply --server-side -f https://raw.githubusercontent.com/cloudnative-pg/cloudnative-pg/release-1.22/releases/cnpg-1.22.2.yaml
    ```